import numpy as np
import pickle
import os
import re
import config
#---------------------------------------------------------------------
# ground truth files made from http://mirtarbase.mbc.nctu.edu.tw/php/search.php#disease
#---------------------------------------------------------------------
thCnt = config.Config.thCnt
sourceFile = config.Config.gt_source
# thCnt = 3
# gt_source = "BRCA_gt.txt"
#---------------------------------------------------------------------
def parse_groundTruth():
    miRNA = []
    mRNA = []
    pair = []
    #---------------------------------------------------------------------
    cnt1, cnt2 = 0, 0
    with open(sourceFile, 'r') as fd:
        for line in fd:
            term = filter(None, re.split(',|\t| \t|\r\n', line))
            # print term
            cnt1 += 1
            if int(term[6]) >= thCnt:
                if not (term[3] in miRNA):
                    miRNA.append(term[3])
                if not (term[4] in mRNA):
                    mRNA.append(term[4])
                cnt2 += 1
                pair.append( (term[3],term[4]) )
    #---------------------------------------------------------------------
    ground_truth = np.zeros((len(miRNA),len(mRNA)))
    gt_mi = []
    gt_m = []
    for i in xrange(len(miRNA)):
        for j in xrange(len(mRNA)):
            p = (miRNA[i], mRNA[j])
            if p in pair:
                ground_truth[i,j] = 1
                gt_mi.append(i)
                gt_m.append(j)
                
    return miRNA, mRNA, ground_truth, gt_mi, gt_m, cnt1, cnt2

