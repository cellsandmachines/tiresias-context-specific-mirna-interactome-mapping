import numpy as np
import pickle
import os
import re
import scipy.stats

import config
miRnaFile_source = config.Config.mirna_exp_source
mRnaFile_source = config.Config.mrna_exp_source
miRnaFile_save = config.Config.mirna_save
mRnaFile_save = config.Config.mrna_save
selected_save = config.Config.selected_exp_save

# miRnaFile_source = 'BRCA_miRNA.csv'
# mRnaFile_source = 'BRCA_gene.csv'
# miRnaFile_save = 'BRCA_miRNA_exp.dat'
# mRnaFile_save = 'BRCA_mRNA_exp.dat'
# selected_save = 'BRCA_selected_exp.txt'

class ExpData:
    def __init__(self, miRNA_list, mRNA_list):
        self.miRNA = {}
        self.mRNA = {}
        self.exp_miRNA = []
        self.exp_mRNA = []
        self.miRNA_list = miRNA_list
        self.mRNA_list = mRNA_list
        
    def readMicroRNA(self):
        if not os.path.isfile(miRnaFile_save): 
            with open(miRnaFile_source, 'r') as fd:
                i = 0
                for line in fd:
                    case = filter(None, re.split(',|\r\n', line))
                    if i == 0:
                        self.miRNA[case[0]] = case[1:]
                        i += 1
                        continue                    
                    exp = []                    
                    for c in case[1:]:
                        if c == 'NaN':
                            exp.append(0.0)
                        else:
                            exp.append(float(c))
                    self.miRNA[case[0]] = exp
                    i += 1

            with open(miRnaFile_save, 'wb') as fdat:
                pickle.dump(self.miRNA, fdat)
        else:
            with open(miRnaFile_save, 'rb') as fdat:
                self.miRNA = pickle.load(fdat)
        print 'miRNA loading done'

    def readMRNA(self):
        if not os.path.isfile(mRnaFile_save): 
            with open(mRnaFile_source, 'r') as fd:
                i = 0
                for line in fd:
                    case = filter(None, re.split(',|\r\n', line))
                    if i == 0:
                        self.mRNA[case[0]] = case[1:]
                        i += 1
                        continue                    
                    self.mRNA[case[0]] = [float(c) for c in case[1:]]
                    i += 1

            with open(mRnaFile_save, 'wb') as fdat:
                pickle.dump(self.mRNA, fdat)
        else:
            with open(mRnaFile_save, 'rb') as fdat:
                self.mRNA = pickle.load(fdat)
        print 'mRNA loading done'
        
    def lookupMiRnaExp(self):
        index = []
        keys = self.miRNA['Case ID']
        for miRNA in self.miRNA_list:
            index.append(keys.index(miRNA))
        print index, self.miRNA_list
        return index


    def lookupMRnaExp(self):
        index = []
        keys = self.mRNA['Entity Submitter ID']
        for mRNA in self.mRNA_list:
            index.append(keys.index(mRNA))
        print index, self.mRNA_list
        return index

    def processExp(self, cases, miRNA_index, mRNA_index):
        num_mi = len(miRNA_index)
        num_m = len(mRNA_index)
        for c in cases:
            exp_mi = np.zeros((1, num_mi))
            exp_m = np.zeros((1, num_m))
            for i in xrange(len(miRNA_index)):
                idx = miRNA_index[i]
                mi_exp = self.miRNA[c][idx]
                exp_mi[0,i] = mi_exp
            for i in xrange(len(mRNA_index)):
                idx = mRNA_index[i]
                exp_m[0,i] = self.mRNA[c][idx]
            self.exp_miRNA.append(np.copy(exp_mi))
            self.exp_mRNA.append(np.copy(exp_m))
            
        return [self.exp_miRNA, self.exp_mRNA, num_mi, num_m]

    def getExp(self):
        self.readMicroRNA()
        self.readMRNA()
        miRNA_index = self.lookupMiRnaExp()
        mRNA_index = self.lookupMRnaExp()
        case = self.miRNA.keys()
        case.remove('Case ID')
        
        return self.processExp(case, miRNA_index, mRNA_index)

    def writeExp(self, num_mi, num_m, num):

    
        fd = open(selected_save, 'w')
        
        all_list = self.miRNA_list + self.mRNA_list
        for i in xrange(len(all_list)):
            sep = ','
            if i == len(all_list)-1:
                sep = '\n'
            fd.write(all_list[i] + sep)
            
        for i in xrange(num):
            sep = ','
            for j in xrange(num_mi):
                fd.write(str(self.exp_miRNA[i][0,j]) + sep)
            for j in xrange(num_m):
                if j == num_m-1:
                    sep = '\n'
                fd.write(str(self.exp_mRNA[i][0,j]) + sep)
        fd.write('\n\n')
        
        miRNA_exp = np.stack(self.exp_miRNA)
        mRNA_exp = np.stack(self.exp_mRNA)
        corr = np.zeros((num_mi, num_m))
        pval = np.zeros((num_mi, num_m))
        for i in xrange(num_mi):    
            sep = ','
            for j in xrange(num_m):    
                if j == num_m-1:
                    sep = '\n'
                r, p = scipy.stats.pearsonr(miRNA_exp[:,0,i], mRNA_exp[:,0,j])
                corr[i, j] = r
                pval[i, j] = p

        temp = ['x']+self.mRNA_list
        for i in xrange(len(temp)):
            sep = ','
            if i == len(temp)-1:
                sep = '\n'
            fd.write(temp[i] + sep)
        
        for i in xrange(num_mi):    
            sep = ','
            fd.write(self.miRNA_list[i] + sep)
            for j in xrange(num_m):    
                if j == num_m-1:
                    sep = '\n'
                fd.write(str(corr[i, j]) + sep)
        fd.write('\n\n')

        temp = ['x']+self.mRNA_list
        for i in xrange(len(temp)):
            sep = ','
            if i == len(temp)-1:
                sep = '\n'
            fd.write(temp[i] + sep)
        
        for i in xrange(num_mi):    
            sep = ','
            fd.write(self.miRNA_list[i] + sep)
            for j in xrange(num_m):    
                if j == num_m-1:
                    sep = '\n'
                fd.write(str(pval[i, j]) + sep)
                
        # print corr
        # print pval
        
        return corr
    
    
    