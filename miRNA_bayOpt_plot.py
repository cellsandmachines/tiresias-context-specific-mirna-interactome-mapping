from bayes_opt import BayesianOptimization
import numpy as np

import matplotlib.pyplot as plt
from matplotlib import gridspec

def posterior(bo, xmin=-2, xmax=10, npoint=500):
    bo.gp.fit(bo.X, bo.Y)
    mu, sigma = bo.gp.predict(np.linspace(xmin, xmax, npoint).reshape(-1, 1), return_std=True)
    return mu, sigma

def plot_gp(bo, x, xmin, xmax, npoint):
    
    # fig = plt.figure(figsize=(16, 10))
    fig = plt.figure()
    # fig.suptitle('Gaussian Process and Utility Function After {} Steps'.format(len(bo.X)), fontdict={'size':30})
    
    gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1]) 
    axis = plt.subplot(gs[0])
    acq = plt.subplot(gs[1])
    
    mu, sigma = posterior(bo, xmin, xmax, npoint)
    # axis.plot(x, y, linewidth=3, label='Target')
    axis.plot(bo.X.flatten(), bo.Y, 'D', markersize=7, label=u'observations', color='r')
    axis.plot(x, mu, '--', color='k', label='prediction')

    axis.fill(np.concatenate([x, x[::-1]]), 
              np.concatenate([mu - 1.9600 * sigma, (mu + 1.9600 * sigma)[::-1]]),
        alpha=.6, fc='c', ec='None', label='95% confidence interval')
    
    axis.set_xlim((xmin, xmax))
    # axis.set_ylim((None, None))
    axis.set_ylabel(r'-$\bar{c}_1(\rho)$', fontdict={'size':20})
    # axis.set_xlabel('x', fontdict={'size':20})
    
    utility = bo.util.utility(x.reshape((-1, 1)), bo.gp, 0)
    acq.plot(x, utility, label='utility', color='purple')
    acq.plot(x[np.argmax(utility)], np.max(utility), 'o', markersize=10, 
             label=u'next guess', markerfacecolor='blue', markeredgecolor='k', markeredgewidth=1)
    acq.set_xlim((xmin, xmax))
    # acq.set_ylim((0, np.max(utility) + 0.5))
    acq.set_ylabel('utility', fontdict={'size':20})
    acq.set_xlabel(r'$\rho$', fontdict={'size':20})
    
    # axis.legend(loc=2, bbox_to_anchor=(1.01, 1), borderaxespad=0.)
    # acq.legend(loc=2, bbox_to_anchor=(1.01, 1), borderaxespad=0.)
    axis.legend(loc='best')
    acq.legend(loc='best')    