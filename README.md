# README #

## List of files ##

* config.py: select the type of data

* config.ini: Specify the names of input files and save files for each data type

* parse_miRTarBase.py: parse ground truth pairs

* parse_TargetScan.py: parse sequence-based prediction results

* miRNA_load_expression: parse expression data

* miRNA_main.py: main file that handles Bayesian ooptimization loop

* miRNA_inference: core implementation of a leanring algorithm

* miRNA_bayOpt_plot: plot Bayesian optimization process


## Config.ini ##

Data type XXX starts with [XXX].
The following variables should be defined.


* thCnt: The number of papers that have confirmed that a pair of an miRNA and an mRNA is interacting.
* gt_source: Ground truth text file downloaded from http://mirtarbase.mbc.nctu.edu.tw/php/search.php#disease
* pred_source: Sequence based prediction results from http://www.targetscan.org/cgi-bin/targetscan/data_download.vert71.cgi (Specifically, one need Conserved_Site_Context_Scores.txt and Nonconserved_Site_Context_Scores.txt, which are not included in this repo)
* pred_save: Parsed data for the sequence based prediction results
* mirna_exp_source: miRNA expression raw data (from TCGA)
* mrna_exp_source: mRNA expression raw data (from TCGA)
* mirna_save: parsed miRNA expression data
* mrna_save: parsed mRNA expression data
* selected_exp_save: A text-form file of miRNA/mRNA expressions, plus Pearson correlation coefficients
* all_data_save: Final-stage parsed data
* bayOpt_result: Bayesian optimization plot
* encoder_save: Autoencoder weights
* model_save: Network model save file prefix
* heatmap: Heat map save file prefix


## How to execute ##

* python miRNA_main.py

Bayseian optimization can be turned off by setting
doBayesianOptimization = False in the "miRNA_main.py".
learningRate and rho should be specified manually in this case.

To activate Bayseian optimization,
we need to install a third-party program from
# https://github.com/fmfn/BayesianOptimization
# http://nbviewer.jupyter.org/github/fmfn/BayesianOptimization/blob/master/examples/visualization.ipynb