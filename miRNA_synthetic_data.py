import numpy as np
import random
import scipy.stats
#-----------------------------------------------------------------------------------------------------
def getStat(miRNA, miRNA_num):
    temp = np.stack(miRNA)
    mean = np.zeros((1, miRNA_num))
    variance = np.zeros((1, miRNA_num))
    for i in xrange(miRNA_num):    
        mean[0, i] = np.mean(temp[:,0,i])
        variance[0, i] = np.var(temp[:,0,i])
    return mean, variance
#-----------------------------------------------------------------------------------------------------       
def writeExp(x_name, y_name, exp_miRNA, exp_mRNA, num_mi, num_m, num, selected_save):


    fd = open(selected_save, 'w')
    
    all_list = x_name + y_name
    for i in xrange(len(all_list)):
        sep = ','
        if i == len(all_list)-1:
            sep = '\n'
        fd.write(all_list[i] + sep)
        
    for i in xrange(num):
        sep = ','
        for j in xrange(num_mi):
            fd.write(str(exp_miRNA[i][0,j]) + sep)
        for j in xrange(num_m):
            if j == num_m-1:
                sep = '\n'
            fd.write(str(exp_mRNA[i][0,j]) + sep)
    fd.write('\n\n')
    
    miRNA_exp = np.stack(exp_miRNA)
    mRNA_exp = np.stack(exp_mRNA)
    corr = np.zeros((num_mi, num_m))
    pval = np.zeros((num_mi, num_m))
    for i in xrange(num_mi):    
        sep = ','
        for j in xrange(num_m):    
            if j == num_m-1:
                sep = '\n'
            r, p = scipy.stats.pearsonr(miRNA_exp[:,0,i], mRNA_exp[:,0,j])
            corr[i, j] = r
            pval[i, j] = p

    temp = ['x']+y_name
    for i in xrange(len(temp)):
        sep = ','
        if i == len(temp)-1:
            sep = '\n'
        fd.write(temp[i] + sep)
    
    for i in xrange(num_mi):    
        sep = ','
        fd.write(x_name[i] + sep)
        for j in xrange(num_m):    
            if j == num_m-1:
                sep = '\n'
            fd.write(str(corr[i, j]) + sep)
    fd.write('\n\n')

    temp = ['x']+y_name
    for i in xrange(len(temp)):
        sep = ','
        if i == len(temp)-1:
            sep = '\n'
        fd.write(temp[i] + sep)
    
    for i in xrange(num_mi):    
        sep = ','
        fd.write(x_name[i] + sep)
        for j in xrange(num_m):    
            if j == num_m-1:
                sep = '\n'
            fd.write(str(pval[i, j]) + sep)
            
    # print corr
    # print pval
    
    return corr        
#-----------------------------------------------------------------------------------------------------    
def genData(nSample,\
            M,\
            N,\
            num_interaction_per_mrna,\
            num_prediction_per_mrna,\
            signal_strength,\
            variance_mirna,\
            variance_mrna):
            
            
    """
        Sythetic data generator

        Arg:
            nSample:    the number of sample pairs to generate
            M:          the number of miRNAs a sample
            N:          the number of mRNAs a sample
            num_interaction_per_mrn:    the number of miRNAs that regulate an mRNA
            num_prediction_per_mrna:    the number of miRNAs that are predicted to regulate an mRNA
            signal_strength:    regulation weight (or strength of regulation)
            variance_mirna:         variance of an miRNA expression
            variance_mrna:          variance of an mRNA expression
        Return:
            x_list:     list of miRNA vectors, each of which is a numpy array of dim(1,M)
            y_list:     list of mRNA vectors, each of which is a numpy array of dim(1,N)
    """
    
    x_list = []
    y_list = []
    gt_mi = []
    gt_m = []
    x_name = [str(i) for i in range(M)]
    y_name = [str(i) for i in range(N)]

    ground_truth = np.zeros((M, N))
    ground_truth_sign = np.zeros((M, N))
    prediction = np.zeros((M, N))
    
    for j in xrange(N):
        all_mirna = range(M)
        pred = random.sample(all_mirna, num_prediction_per_mrna)
        gt = random.sample(pred, num_interaction_per_mrna)
        for i in xrange(M):
            if i in pred:
                prediction[i,j] = 1
            if i in gt:
                ground_truth[i,j] = 1
                # ground_truth_sign[i,j] = random.choice((-1.0,1.0))
                if random.random() < 0.1:
                    ground_truth_sign[i,j] = 1
                else:
                    ground_truth_sign[i,j] = -1
                    
                gt_mi.append(i)
                gt_m.append(j)
            
    # print "ground_truth = \n", ground_truth
    
    for s in xrange(nSample):
        x = np.zeros((1, M))
        for i in xrange(M):
            x[0,i] = np.random.normal(0, variance_mirna)
        x_list.append(x)
    mean, var = getStat(x_list, M)
    x_list_s = np.divide(x_list-mean, np.sqrt(var)) + 3*np.ones((1,1)) # N(3, 1)
    
    
    for s in xrange(nSample):        
        y = np.zeros((1, N))
        for j in xrange(N):
            gt = ground_truth[:, j]
            idx = [i for i in xrange(M) if gt[i] == 1.0]
            regulation = 0.0

            for g in idx:
                regulation += ground_truth_sign[g,j] * signal_strength * x_list_s[s][0,g]
            y[0,j] = np.random.normal(10 + regulation, np.sqrt(variance_mrna)) # N(10+reg, 1)
        
        y_list.append(y)    

    return x_list, y_list, x_name, y_name, ground_truth, gt_mi, gt_m, prediction, ground_truth_sign
 