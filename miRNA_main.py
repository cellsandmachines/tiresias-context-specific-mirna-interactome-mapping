import config
config.Config.select_menu()
#---------------------------------------------------------------------
import numpy as np
import os
import pickle
import matplotlib.pyplot as plt
#-----------------------------------------------------------------------------------------------------
import miRNA_synthetic_data as syn
import parse_miRTarBase as gt
import parse_TargetScan as pred
import miRNA_load_expression as data_exp
import miRNA_inference as infer
# https://github.com/fmfn/BayesianOptimization
# http://nbviewer.jupyter.org/github/fmfn/BayesianOptimization/blob/master/examples/visualization.ipynb
from bayes_opt import BayesianOptimization
import miRNA_bayOpt_plot as bayOptPlot
#-----------------------------------------------------------------------------------------------------
nStep = 100
nData = 1000
nBatch = 1
    
learningRate = 0.005 
rho = 0.25

nInput = 5
nOutput = 5

doBayesianOptimization = True
# doBayesianOptimization = False
#-----------------------------------------------------------------------------------------------------
def processResult(bo, key):
    x = [bo.res['all']['params'][i][key] for i in xrange(len(bo.res['all']['params']))]
    y = bo.res['all']['values']
    print repr( sorted(zip(x, y), key=lambda arg: arg[1]) )
#-----------------------------------------------------------------------------------------------------
def getStat(mRNA, mRNA_num):
    temp = np.stack(mRNA)
    mean = np.zeros((1, mRNA_num))
    variance = np.zeros((1, mRNA_num))
    for i in xrange(mRNA_num):    
        mean[0, i] = np.mean(temp[:,0,i])
        variance[0, i] = np.var(temp[:,0,i])
    return mean, variance
#-----------------------------------------------------------------------------------------------------
def normalize(miRNA, mRNA, nInput, nOutput):
    mean, var = getStat(miRNA, nInput)
    # print mean
    # print var
    miRNA = np.divide(miRNA-mean, np.sqrt(var)) + 3*np.ones((1,1))
    mean, var = getStat(miRNA, nInput)
    # print mean
    # print var    
    mRNA_mean, mRNA_var = getStat(mRNA, nOutput)
    # print mRNA_mean
    # print mRNA_var    
    mRNA = np.divide(mRNA, mRNA_mean) + 10*np.ones((1,1))
    mRNA_mean, mRNA_var = getStat(mRNA, nOutput)
    # print mRNA_mean
    # print mRNA_var
    
    return miRNA, mRNA, mRNA_mean, mRNA_var
#-----------------------------------------------------------------------------------------------------
        
if __name__ == '__main__':

    dataFile = config.Config.all_data_save
    bayOpt_result = config.Config.bayOpt_result
    
    miRNA, mRNA = [], []
    if not os.path.isfile(dataFile): 
        if config.Config.key == 'SYNTHETIC':
        
            
            miRNA, mRNA, miRNA_list, mRNA_list, gt, gt_mi, gt_m, prediction, gt_sign = syn.genData(\
                nData,\
                config.Config.num_mirna,\
                config.Config.num_mrna,\
                config.Config.num_interaction_per_mrna,\
                config.Config.num_prediction_per_mrna,\
                config.Config.signal_strength,\
                config.Config.variance_mirna,\
                config.Config.variance_mrna)
                
            nInput = config.Config.num_mirna
            nOutput = config.Config.num_mrna

            corr = syn.writeExp(miRNA_list, mRNA_list, miRNA, mRNA, nInput, nOutput, nData, config.Config.selected_exp_save)                 
            
        else:
            miRNA_list, mRNA_list, gt, gt_mi, gt_m, _, __ = gt.parse_groundTruth()
            prediction = pred.parse_prediction(miRNA_list, mRNA_list)
            d = data_exp.ExpData(miRNA_list, mRNA_list)
            ret = d.getExp()

            miRNA = ret[0]
            mRNA = ret[1]
            nInput = ret[2]
            nOutput = ret[3]
            nData = len(miRNA)    
            
            corr = d.writeExp(nInput, nOutput, nData)
            
        if config.Config.save_enable == "Enable":
            with open(dataFile, 'wb') as fdat:
                pickle.dump((miRNA, mRNA, nInput, nOutput, miRNA_list, mRNA_list, nData, gt, gt_mi, gt_m, prediction, corr), fdat)  
    else:
        miRNA, mRNA, nInput, nOutput, miRNA_list, mRNA_list, nData, gt, gt_mi, gt_m, prediction, corr = pickle.load( open( dataFile, "rb" ) )


    miRNA, mRNA, mRNA_mean, mRNA_var = normalize(miRNA, mRNA, nInput, nOutput)
    infer.init(nStep, nData, nBatch, nInput, nOutput, miRNA, mRNA, [mRNA_mean, mRNA_var], miRNA_list, mRNA_list, [gt_mi, gt_m, prediction, corr], True)
    
    
    if doBayesianOptimization == True:
        target = lambda rho: infer.runTraining(learningRate, rho, preTrain = False)[0]

        xMin = 0.02
        xMax = 0.60
        nPoint = 500
        bo = BayesianOptimization(target, {'rho': (xMin, xMax)})
        # gp_params = {'corr': 'cubic'}
        # bo.explore({'rho': [0.07, 0.25, 0.35]})
        bo.explore({'rho': [0.03, 0.3, 0.5]})        
        bo.maximize(init_points=0, n_iter=10, acq='ucb', kappa=3)#, **gp_params)
        print(bo.res['max'])
        print(bo.res['all'])
        processResult(bo, 'rho')
        
        x = np.linspace(xMin, xMax, nPoint)
        bayOptPlot.plot_gp(bo, x, xMin, xMax, nPoint)
        plt.savefig(bayOpt_result)

            
    else:
        _, ret = infer.runTraining(learningRate, rho, preTrain = False)
        
    # print 'corr = \n', corr
    cnt = len(gt_mi)
    print 'ground truth = \n', gt, cnt, nInput*nOutput, 100.0*cnt/float(nInput*nOutput)
    
