import ConfigParser
config_file = "config.ini"
data_set = ('SYNTHETIC','BRCA', 'LUAD', 'UCEC')
#----------------------------------------------------------------
class Config:
    thCnt = None
    gt_source = None
    pred_source = None
    pred_save = None
    mirna_exp_source = None
    mrna_exp_source = None
    mirna_save = None
    mrna_save = None
    selected_exp_save = None
    all_data_save = None
    bayOpt_result = None
    encoder_save = None
    model_save = None
    heatmap = None
    key = None
    save_enable = None

    # Used only for synthetic data case.
    num_mirna = None
    num_mrna = None
    num_interaction_per_mrna = None
    num_prediction_per_mrna = None
    signal_strength = None
    variance_mirna = None
    variance_mrna = None
    

    @staticmethod
    def select_menu():
       
        default = '0'
        while True:
            print("-------------------------------")
            print(" 0 - Synthetic Data")
            print(" 1 - TCGA-BRCA")
            print(" 2 - TCGA-LUAD")
            print(" 3 - TCGA-UCEC")
            print("-------------------------------")        
            t = raw_input("Enter a number to choose data (default="+default+"): ") or default
        
            try:
                idx = int(t)
                key = data_set[idx]
                print key + ' selected'
                Config.key = key
                break
            except:
                print("\n\nError : Please enter a valid number!!\n\n") 


        c = ConfigParser.ConfigParser()
        c.read(config_file)
        
        if key == 'SYNTHETIC':
            Config.num_mirna = int( c.get(key, 'num_mirna') )
            Config.num_mrna = int( c.get(key, 'num_mrna') )
            Config.num_interaction_per_mrna = int( c.get(key, 'num_interaction_per_mrna') )
            Config.num_prediction_per_mrna = int( c.get(key, 'num_prediction_per_mrna') )
            Config.signal_strength = float( c.get(key, 'signal_strength') )
            Config.variance_mirna = float( c.get(key, 'variance_mirna') )
            Config.variance_mrna = float( c.get(key, 'variance_mrna') )
            
            Config.mirna_save = c.get(key, 'mirna_save')
            Config.mrna_save = c.get(key, 'mrna_save')
            Config.selected_exp_save = c.get(key, 'selected_exp_save')            
            Config.all_data_save = c.get(key, 'all_data_save')
            Config.bayOpt_result = c.get(key, 'bayOpt_result')
            Config.encoder_save = c.get(key, 'encoder_save')
            Config.model_save = c.get(key, 'model_save')
            Config.heatmap = c.get(key, 'heatmap')
            Config.save_enable = c.get(key, 'save_enable')
            
        else:
            Config.thCnt = int( c.get(key, 'thCnt') )
            Config.gt_source = c.get(key, 'gt_source')
            s = c.get(key, 'pred_source')
            Config.pred_source = s.split(',')
            Config.pred_save = c.get(key, 'pred_save')
            Config.mirna_exp_source = c.get(key, 'mirna_exp_source')
            Config.mrna_exp_source = c.get(key, 'mrna_exp_source')
            Config.mirna_save = c.get(key, 'mirna_save')
            Config.mrna_save = c.get(key, 'mrna_save')
            Config.selected_exp_save = c.get(key, 'selected_exp_save')
            Config.all_data_save = c.get(key, 'all_data_save')
            Config.bayOpt_result = c.get(key, 'bayOpt_result')
            Config.encoder_save = c.get(key, 'encoder_save')
            Config.model_save = c.get(key, 'model_save')
            Config.heatmap = c.get(key, 'heatmap')
            Config.save_enable = c.get(key, 'save_enable')

        return
#----------------------------------------------------------------
if __name__ == '__main__':  
    Config.select_menu()
    print Config.thCnt
    print Config.gt_source
    print Config.pred_source



